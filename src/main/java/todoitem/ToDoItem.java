package todoitem;

import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;
import org.axonframework.eventsourcing.annotation.AggregateIdentifier;
import todoitem.command.CreateToDoItemCommand;
import todoitem.command.MarkCompletedCommand;
import todoitem.command.ToDoItemCreatedEvent;
import todoitem.event.ToDoItemCompletedEvent;

public class ToDoItem extends AbstractAnnotatedAggregateRoot {

    @AggregateIdentifier
    private String todoId;
    private String description;

    public ToDoItem() {
    }

    @CommandHandler
    public ToDoItem(CreateToDoItemCommand createCommand) {
        apply(new ToDoItemCreatedEvent(createCommand.getTodoId(), createCommand.getDescription()));
    }

    @CommandHandler
    public void markItemAsCompleted(MarkCompletedCommand markCompletedCommand) {
        apply(new ToDoItemCompletedEvent(markCompletedCommand.getTodoId()));
    }

    @EventHandler
    public void on(ToDoItemCreatedEvent event) {
        todoId = event.getTodoId();
    }


}
