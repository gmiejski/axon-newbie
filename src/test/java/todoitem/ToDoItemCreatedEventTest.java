package todoitem;

import org.axonframework.test.FixtureConfiguration;
import org.axonframework.test.Fixtures;
import org.junit.Before;
import org.junit.Test;
import todoitem.command.CreateToDoItemCommand;
import todoitem.command.MarkCompletedCommand;
import todoitem.command.ToDoItemCreatedEvent;
import todoitem.event.ToDoItemCompletedEvent;

public class ToDoItemCreatedEventTest {

    private FixtureConfiguration fixture;

    @Before
    public void setUp() throws Exception {
        fixture = Fixtures.newGivenWhenThenFixture(ToDoItem.class);
    }

    @Test
    public void createToDoItem() throws Exception {
        String desc = "desc";
        String id = "id";

        fixture.given().when(new CreateToDoItemCommand(id, desc)).expectEvents(new ToDoItemCreatedEvent(id, desc));
    }

    @Test
    public void markToDoItemAsCompleted() throws Exception {
        String desc = "desc";
        String id = "id";

        fixture.given(new ToDoItemCreatedEvent(id, desc)).when(new MarkCompletedCommand(id)).expectEvents(new ToDoItemCompletedEvent(id));
    }
}